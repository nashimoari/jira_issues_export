import requests
import json
import math

LOGIN = ''
PASS = ''
PROJECT_NAME = ''

def get_issues_by_page(page_id, cnt_per_page):
    url = f'https://{LOGIN}:{PASS}@jira.designplanet.ua/rest/api/2/search'
    
    params = {
        'jql':f'project={PROJECT_NAME}',
        'maxResults':cnt_per_page,
    }

    if page_id > 0:
        params['startAt'] = cnt_per_page * page_id

    resp = requests.get(url, params)
    if not resp.ok:
        print(resp.status_code)
        print(resp.content)
        raise Exception('something goes wrong')

    # Calculate pages count
    json_resp = json.loads(resp.content)

    return json_resp['total'], json_resp['issues']


def get_issue_details(issue_id):
    url = f'https://{LOGIN}:{PASS}@jira.designplanet.ua/rest/api/2/issue/{issue_id}'

    resp = requests.get(url)
    if not resp.ok:
        print(resp.status_code)
        print(resp.content)
        raise Exception('something goes wrong')
    
    return json.loads(resp.content)


is_finish_page = False
page_id = 1
cnt_per_page = 500

while not is_finish_page:
    total, issues = get_issues_by_page(page_id, cnt_per_page)

    pages_cnt = math.ceil(total / cnt_per_page)
    print('total issues:', total)
    print('total pages count: ',pages_cnt)
    print('current page id:', page_id)

    if page_id >= pages_cnt:
        is_finish_page = True

    issue_cnt = 1
    for issue in issues:
        issue_id = issue['id']
        
        # save issue main info 
        with open(f'issues/issue_{issue_id}.json', 'wb') as f:
            f.write(json.dumps(issue, ensure_ascii=False).encode('utf8'))

        
        # save issue details
        issue_details = get_issue_details(issue_id)

        with open(f'issues/issue_{issue_id}_details.json', 'wb') as f:
            f.write(json.dumps(issue_details, ensure_ascii=False).encode('utf8'))
        
        print('total issues:', total, '; total pages count: ',pages_cnt, '; current page id:', page_id, '; current issue id:', issue_id, '; issue_cnt_on_page:', issue_cnt)
        issue_cnt +=1

    page_id += 1